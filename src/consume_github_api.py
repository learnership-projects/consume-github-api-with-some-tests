import os
from datetime import datetime

import requests


GITHUB_TOKEN = os.getenv("GITHUB_TOKEN", default=None)
BASE_URL = "https://api.github.com"


def validate_date(date_string):
    try:
        new_date_string = datetime.strptime(date_string, "%Y-%m-%d")
        return new_date_string
    except ValueError:
        raise ValueError(
            "Invalid date format. Please use 'YYYY-MM-DD' format for dates."
        )


def validate_owner_existence(owner):
    url = f"{BASE_URL}/users/{owner}"
    response = requests.get(url)

    if response.status_code == 404 or response.status_code != 200:
        raise ValueError(f"Repository owner {owner} not found.")


def validate_repository_existence(owner, repo, headers):
    url = f"{BASE_URL}/repos/{owner}/{repo}"
    response = requests.get(url, headers=headers)

    if response.status_code == 404 or response.status_code != 200:
        raise ValueError(f"Repo {repo} not found.")


def is_date_within_range(date_string, start_date, end_date):
    return (
        date_string is not None
        and start_date <= datetime.strptime(date_string[:10], "%Y-%m-%d") <= end_date
    )


def is_pull_request_created_updated_merged_or_closed_in_date_range(
    pull_request, start_date, end_date
):
    merged_at = pull_request.get("merged_at", None)
    closed_at = pull_request.get("closed_at", None)
    updated_at = pull_request.get("updated_at", None)
    created_at = pull_request.get("created_at", None)

    merged_at_range = is_date_within_range(merged_at, start_date, end_date)
    closed_at_range = is_date_within_range(closed_at, start_date, end_date)
    updated_at_range = is_date_within_range(updated_at, start_date, end_date)
    created_at_range = is_date_within_range(created_at, start_date, end_date)

    return any([merged_at_range, closed_at_range, updated_at_range, created_at_range])


def generate_headers():
    headers = {
        "Accept": "application/vnd.github+json",
        "X-GitHub-Api-Version": "2022-11-28",
    }
    if GITHUB_TOKEN is not None:
        headers["Authorization"] = f"Bearer {GITHUB_TOKEN}"

    return headers


def extract_pull_request_info(pull_request):
    return {
        "id": pull_request["id"],
        "user": pull_request["user"]["login"],
        "title": pull_request["title"],
        "state": pull_request["state"],
        "created_at": pull_request["created_at"][:10],
    }


def get_sorted_pull_requests(pull_requests):
    sorted_pull_requests = sorted(
        pull_requests, key=lambda item: item["created_at"], reverse=True
    )
    return sorted_pull_requests


def get_pull_requests(owner, repo, start_date, end_date):
    session = requests.session()
    headers = generate_headers()
    session.headers.update(headers)

    start_date = validate_date(start_date)
    end_date = validate_date(end_date)
    if start_date > end_date:
        raise ValueError("Start date cannot be after end date.")

    validate_owner_existence(owner)
    validate_repository_existence(owner, repo, headers)

    pull_request_url = f"{BASE_URL}/repos/{owner}/{repo}/pulls?state=all&per_page=100"
    pull_requests = []
    while pull_request_url:
        response = session.get(pull_request_url)

        if response.status_code != 200:
            raise ValueError(f"Error: {response.text}")

        response_json = response.json()
        pull_requests_per_page = [
            extract_pull_request_info(pull_request)
            for pull_request in response_json
            if is_pull_request_created_updated_merged_or_closed_in_date_range(
                pull_request, start_date, end_date
            )
        ]

        pull_requests += pull_requests_per_page
        link_header = response.links
        if "next" in link_header:
            pull_request_url = link_header["next"]["url"]
        else:
            pull_request_url = None

    return get_sorted_pull_requests(pull_requests)
