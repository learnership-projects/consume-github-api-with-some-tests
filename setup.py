from setuptools import setup, find_packages

setup(
    name="src",
    version="1.0.0",
    decription="This package gets a list of pull requests of the repository which were created, updated, merged or closed between the given two dates.",
    author="Kholofelo Moropane",
    packages=find_packages(),
)