import unittest
from unittest.mock import patch, Mock

from src.consume_github_api import get_pull_requests, generate_headers


class TestGetPullRequests(unittest.TestCase):
    @patch("src.consume_github_api.requests.session")
    @patch("src.consume_github_api.requests.get")
    @patch("src.consume_github_api.os.getenv")
    def test_returns_correct_data_format(
        self,
        mocked_getenvvar,
        mocked_requests_get,
        mocked_requests_session,
    ):
        mock_requests_response = Mock()
        mock_requests_response.status_code = 200
        mock_requests_response.json.return_value = [
            {
                "closed_at": None,
                "created_at": "2023-11-06T14:51:23Z",
                "id": 1588796386,
                "merged_at": None,
                "state": "open",
                "title": "Fixed mocking_return_value2.py",
                "updated_at": "2023-11-06T14:39:45Z",
                "url": "https://api.github.com/repos/rkmoropane/IMPROVE-TESTING-WITH-MOCKING-OBJECT/pulls/2",
                "user": {"login": "rkmoropane"},
            },
        ]

        mocked_requests_get.return_value = mock_requests_response

        mocked_requests_session.return_value.get.return_value = mock_requests_response
        mocked_requests_session.return_value.get.return_value.links = {}

        mocked_getenvvar.return_value = None

        result = get_pull_requests(
            "rkmoropane",
            "IMPROVE-TESTING-WITH-MOCKING-OBJECT",
            "2023-10-30",
            "2023-11-07",
        )

        expected_headers = generate_headers()
        expected_url = f"https://api.github.com/repos/rkmoropane/IMPROVE-TESTING-WITH-MOCKING-OBJECT/pulls?state=all&per_page=100"

        mocked_requests_session().headers.update.assert_called_once_with(
            expected_headers
        )
        mocked_requests_session().get.assert_called_once_with(expected_url)

        expected_result = [
            {
                "created_at": "2023-11-06",
                "id": 1588796386,
                "state": "open",
                "title": "Fixed mocking_return_value2.py",
                "user": "rkmoropane",
            }
        ]

        self.assertEqual(result, expected_result)

    @patch("src.consume_github_api.requests.get")
    def test_throws_error_if_owner_doesnt_exist(self, mocked_requests_get):
        mocked_response = Mock()
        mocked_response.status_code = 404

        mocked_requests_get.return_value = mocked_response

        with self.assertRaises(ValueError) as context:
            result = get_pull_requests(
                "GregBerryGreenMamba", "ACN-Syllabus", "2023-10-16", "2023-11-05"
            )

        expected_url = f"https://api.github.com/users/GregBerryGreenMamba"
        mocked_requests_get.assert_called_once_with(expected_url)
        self.assertIn(
            f"Repository owner GregBerryGreenMamba not found.", str(context.exception)
        )

    @patch("src.consume_github_api.requests.get")
    @patch("src.consume_github_api.os.getenv")
    @patch("src.consume_github_api.validate_owner_existence")
    def test_throws_error_if_repo_doesnt_exist(
        self, mocked_validate_owner_existence, mocked_getenvvar, mocked_requests_get
    ):
        mocked_response = Mock()
        mocked_response.status_code = 404

        mocked_getenvvar.return_value = None
        mocked_validate_owner_existence.return_value = True
        mocked_requests_get.return_value = mocked_response

        with self.assertRaises(ValueError) as context:
            result = get_pull_requests(
                "rkmoropane", "calculator", "2023-10-25", "2023-11-08"
            )

        expected_url = f"https://api.github.com/repos/rkmoropane/calculator"
        expected_header = generate_headers()

        mocked_validate_owner_existence.assert_called_once_with("rkmoropane")
        mocked_requests_get.assert_called_once_with(
            expected_url, headers=expected_header
        )
        self.assertIn(f"Repo calculator not found.", str(context.exception))


if __name__ == "__main__":
    unittest.main()
