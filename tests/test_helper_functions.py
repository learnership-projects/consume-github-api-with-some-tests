import unittest
from datetime import datetime

from src.consume_github_api import (
    validate_date,
    is_date_within_range,
    is_pull_request_created_updated_merged_or_closed_in_date_range,
    get_sorted_pull_requests,
)


class TestConsumeGithubApiHelperFunctions(unittest.TestCase):
    def test_validate_date_returns_datetime_date(self):
        output_date = validate_date("2023-10-08")

        self.assertEqual(output_date, datetime(2023, 10, 8))

    def test_validate_date_incorrect_date_format(self):
        with self.assertRaises(ValueError) as context:
            output_date = validate_date("2023-10-01T25:39:45Z")

        self.assertIn(
            "Invalid date format. Please use 'YYYY-MM-DD' format for dates",
            str(context.exception),
        )

    def test_is_date_within_range_falls_within_date_range(self):
        expected_output = True
        output_check = is_date_within_range(
            "2022-06-30T14:39:45Z", datetime(2022, 6, 30), datetime(2022, 7, 1)
        )

        self.assertEqual(output_check, expected_output)

    def test_is_date_within_range_not_fall_within_date_range(self):
        expected_output = False
        output_check = is_date_within_range(
            "2022-07-03T19:19:13Z", datetime(2022, 6, 30), datetime(2022, 7, 1)
        )

        self.assertEqual(output_check, expected_output)

    def test_is_pull_request_created_updated_merged_or_closed_in_date_range_success_scenario(
        self,
    ):
        input_pull_request = {
            "closed_at": None,
            "created_at": "2023-11-01T09:42:15Z",
            "id": 1588796386,
            "merged_at": None,
            "state": "open",
            "title": "Fixed test_helper_functions.py",
            "updated_at": "2023-11-07T14:39:45Z",
            "url": "https://api.github.com/repos/rkmoropane/Kholofelo-Moropane-186-consume-github-api-python/pulls/3",
            "user": {"login": "rkmoropane"},
        }

        matching_prs_present = (
            is_pull_request_created_updated_merged_or_closed_in_date_range(
                input_pull_request, datetime(2023, 10, 19), datetime(2023, 11, 8)
            )
        )

        self.assertEqual(matching_prs_present, True)

    def test_is_pull_request_created_updated_merged_or_closed_in_date_range_pr_not_matched_failure_scenario(
        self,
    ):
        input_pull_request = {
            "closed_at": None,
            "created_at": "2023-10-20T10:10:30Z",
            "id": 1784765328,
            "merged_at": "2023-10-23T15:20:35Z",
            "state": "closed",
            "title": "Fixed test_helper_functions.py",
            "updated_at": "2023-10-21T14:39:45Z",
            "url": "https://api.github.com/repos/rkmoropane/Kholofelo-Moropane-186-consume-github-api-python/pulls/2",
            "user": {"login": "rkmoropane"},
        }
        matching_prs_present = (
            is_pull_request_created_updated_merged_or_closed_in_date_range(
                input_pull_request, datetime(2023, 9, 30), datetime(2023, 9, 30)
            )
        )

        self.assertEqual(matching_prs_present, False)

    def test_get_sorted_pull_requests_returns_sorted_prs(self):
        input_pull_requests = [
            {
                "created_at": "2023-11-01T09:42:15Z",
                "id": 1588796386,
                "state": "open",
                "title": "Fixed test_helper_functions.py",
                "user": "rkmoropane",
            },
            {
                "created_at": "2023-10-20T10:10:30Z",
                "id": 1784765328,
                "state": "closed",
                "title": "Fixed test_helper_functions.py",
                "user": "rkmoropane",
            },
        ]

        output_prs = get_sorted_pull_requests(input_pull_requests)

        expected_sorted_pull_requests = [
            {
                "created_at": "2023-11-01T09:42:15Z",
                "id": 1588796386,
                "state": "open",
                "title": "Fixed test_helper_functions.py",
                "user": "rkmoropane",
            },
            {
                "created_at": "2023-10-20T10:10:30Z",
                "id": 1784765328,
                "state": "closed",
                "title": "Fixed test_helper_functions.py",
                "user": "rkmoropane",
            },
        ]

        self.assertEqual(output_prs, expected_sorted_pull_requests)


if __name__ == "__main__":
    unittest.main()
