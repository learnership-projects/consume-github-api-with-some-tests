# Consume Github API

## What's the purpose of this package:
___
1. **Main function `get_pull_requests`:**
- The `get_pull_requests` is the main function of this script. This main function takes in the following arguments - The owner, repository name, start date in the format (YYYY-MM-DD) and end date in the format (YYYY-MM-DD). It should return a list or array of pull requests of the repository name taken in as arguments, the list of PRs returned are the ones that were created, updated, merged or closed between the given two dates. 
- The return data should be in the following format:
```
# input
get_pull_requests("Umuzi-org", "ACN-syllabus", "2022-03-01", "2022-03-10")

# output
[
  {"id": 876359209, "user":"FaithMo", "title":"added data sci and eng info", "state":"open", "created_at":"2022-03-10"},
  {"id": 874927260, "user":"ry-oc", "title":"update sololearn python and all contentlinks etc", "state":"closed", "created_at": "2022-03-09"},
  {"id": 872630389, "user":"Andy-Nkumane", "title":"added clarity on python error raising", "state":"open", "created_at":"2022-03-07"},
  {"id": 872484561, "user":"Kate-bit-dev", "title":"Update _index.md", "state":"closed", "created_at":"2022-03-06"},
  {"id": 872482562, "user":"Kate-bit-dev", "title":"Update _index.md", "state":"open", "created_at":"2022-03-06"},
  {"id": 872481470, "user":"Kate-bit-dev", "title":"Update _index.md", "state":"closed", "created_at":"2022-03-06"},
  {"id": 872480774, "user":"Kate-bit-dev", "title":"Update _index.md", "state":"closed", "created_at":"2022-03-06"},
  {"id": 872480210, "user":"Kate-bit-dev", "title":"Update _index.md", "state":"closed", "created_at":"2022-03-06"},
  ...
]
```
2. **Helper functions:**
- The helper function called `validate_date` validates the start and end dates provided as arguments to `get_pull_requests`. After validating the dates, it returns a properly formatted date string.
- The helper function called `validate_owner_existence` takes in an owner name and verifies its existence as a GitHub username.
- The helper function called `validate_repositories_existence` takes in parameter owner, repository, and headers and it validates whether the specified repository name exists on GitHub under the provided owner."

## How to run this package.
___
1. **Start by opening your Ubuntu terminal in Visual Studio Code and create your virtual environment:**
```
pipenv install
```
Activate your virtual environment:
```
pipenv shell
``` 
Ensure that your virtual environment is up to date. Install all the required dependencies to run this package by executing the following command:

```
pipenv install -r requirements.txt
```
This command installs the necessary packages listed in the `requirements.txt` file.

2. **Install the package in the development mode in your terminal:**
```
python setup.py develop
```

3. **Create your personal GitHub token in your GitHub Account.**
- Go to the settings, access Developer Settings in the left sidebar, and click on "Developer settings.". From the Developer settings menu, click on "Personal access tokens." Click on the "Generate new token" button. Enter a suitable name for your token in the "Note" field. This name is for your reference, to identify the token's purpose. Choose the scopes (permissions) you want to grant to this token. Select the appropriate checkboxes based on what you intend to use the token for. For API access, you might need to select at least the "repo" scope.". After selecting the required scopes, click the "Generate token" button at the bottom of the page. Once the token is generated, you will see a screen displaying the token. Copy this token immediately and store it in a secure place. GitHub will not show it to you again.

4. **Create a new shell script and name it `github_token.sh`. In this script, you will define the secret environment variable `GITHUB_TOKEN` and store your personal GitHub token within it. Copy and paste the following content into your shell script:**
```
#!/bin/sh

export GITHUB_TOKEN=YOUR_TOKEN_HERE
```
Replace YOUR_TOKEN_HERE with the GitHub personal token you have generated. Save the shell file, go to your Ubuntu terminal and run your shell script `github_token.sh` with the following commands:
```
source github_token.sh
```
Done, your GitHub personal token is stored under the environmental variable `GITHUB_TOKEN`.
You should be able to see it in your terminal if you run the following commands:
```
echo $GITHUB_TOKEN
```
5. **Now go to your Ubuntu terminal and run the module by running the following commands:**
```
python3 src/consume_github_api.py
```
6. **To run the tests, run the following commands:**
```
python tests/test_helper_functions.py
python tests/test_consume_github_api.py
```
- [x] The tests passed successfully
